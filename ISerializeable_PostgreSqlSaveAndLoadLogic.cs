﻿using Meep.Tech.Data.Attributes.Models;
using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Meep.Tech.Data.Serialization {

  /// <summary>
  /// Plugin to help Serializer serialize to a remote postgreSQL db.
  /// </summary>
  public static class ISerializeable_PostgreSqlSaveAndLoadLogic {

    /// <summary>
    /// Settings to set before configuring.
    /// </summary>
    public static class Settings {

      /// <summary>
      /// The main DB name that this saves to
      /// </summary>
      public static string DBName = "Models.db";
      public static string DBHost = "localhost";
      public static string DBUser = "";
      public static string DBPass = "";
    }

    /// <summary>
    /// Used to split columns in a list
    /// </summary>
    public const string SqlListDelimiter = ", \n\t";

    /// <summary>
    /// Used to map C# data types to SQL field table types
    /// </summary>
    public static Dictionary<Type, string> PostgreSqlColumnDataTypeMappings {
      get;
    } = new Dictionary<Type, string> {
      {typeof(byte[]), "BLOB" },
      {typeof(int), "INTEGER" },
      {typeof(byte), "INTEGER" },
      {typeof(bool), "INTEGER" },
      {typeof(short), "INTEGER" },
      {typeof(string), "TEXT" },
      {typeof(DateTime), "TEXT" },
      {typeof(decimal), "TEXT" },
      {typeof(float), "REAL" },
      {typeof(double), "REAL" }
    };

    /// <summary>
    /// Used to map C# data types to SQL field table types
    /// </summary>
    public static Dictionary<Type, System.Data.DbType> SQLColumnDataTypeMappings {
      get;
    } = new Dictionary<Type, System.Data.DbType> {
      {typeof(byte[]), System.Data.DbType.Binary },
      {typeof(int), System.Data.DbType.Int64 },
      {typeof(byte), System.Data.DbType.Byte },
      {typeof(bool), System.Data.DbType.Boolean },
      {typeof(short), System.Data.DbType.Int16 },
      {typeof(string), System.Data.DbType.String },
      {typeof(DateTime), System.Data.DbType.DateTime },
      {typeof(decimal), System.Data.DbType.Decimal },
      {typeof(float), System.Data.DbType.Decimal },
      {typeof(double), System.Data.DbType.Decimal }
    };

    /// <summary>
    /// The db connection
    /// </summary>
    public static NpgsqlConnection ModelDBConnection {
      get;
      internal set;
    }

    /// <summary>
    /// Call before using the serializer.
    /// </summary>
    public static void Configure() {
      string connectionString =
        $"Host={Settings.DBHost};Database={Settings.DBName};"
        + (!string.IsNullOrEmpty(Settings.DBUser)
          ? $"Username={ Settings.DBUser };"
          : "")
        + (!string.IsNullOrEmpty(Settings.DBPass)
          ? $"Password={ Settings.DBPass };"
          : "");

      ModelDBConnection = new NpgsqlConnection(
        connectionString
      );
      ModelDBConnection.Open();

      Serializer.Settings.Storage.FetchDataForModelLogic = GetModelData;
      Serializer.Settings.Storage.FetchDataForChildModelsLogic = GetDataForChildModels;
      Serializer.Settings.Storage.FetchInfoForLinkedModelsLogic = GetLinkedModelsInfo;
      Serializer.Settings.Storage.FetchDataForModelImageLogic = GetDataForModelImage;
      Serializer.Settings.Storage.SaveDatasForModelsLogic = SaveModelDatas;
      Serializer.Settings.Storage.FetchDataForAllModelsUnsafeSelectLogic 
        = (select, modelType) => GetDataForModels(
          modelType, 
          fullSelect: select
        );
      Serializer.Settings.Storage.FetchDataForAllModelsWhereAddendumLogic
        = (whereClause, modelType, extraParams) => GetDataForModels(
          modelType, 
          whereAddendum: whereClause,
          extraParams: extraParams
        );
    }

    #region DB Access

    /// <summary>
    /// Get the model based on ID without child data
    /// </summary>
    static Serializer.SerializedData? GetModelData(string modelUniqueId, Type modelType) {
      string modelTableName = Serializer.Cache.GetDataModelInfo(modelType).TableName;
      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> childModelFieldsInfo
        = Serializer.Cache.GetFieldsInfo(modelType);
      IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> sqlFieldsForThisModelType
        = childModelFieldsInfo.Where(field => field.dataModelFieldInfo.IsASQLField);

      return GetModelData(modelUniqueId, modelTableName, sqlFieldsForThisModelType);
    }

    /// <summary>
    /// Get the model based on ID without child data
    /// </summary>
    static Serializer.SerializedData? GetModelData(
      string modelUniqueId,
      string modelTableName,
      IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> sqlFieldsForThisModelType
    ) {
      NpgsqlCommand query = ModelDBConnection.CreateCommand();
      (IEnumerable<string> sqlColumns, string tableAssuranceQuery, _) = GetTableAssuranceQuery(modelTableName, sqlFieldsForThisModelType);
      query.CommandText = tableAssuranceQuery
        + "SELECT " +
            sqlColumns.Join(SqlListDelimiter) + @"
          FROM " + modelTableName + " " +
         "WHERE id = @id";
      query.Parameters.AddWithValue("id", modelUniqueId);
      NpgsqlDataReader modelDataRow = query.ExecuteReader();

      // on failure:
      if(!modelDataRow.HasRows) {
        modelDataRow.Close();
        query.Dispose();
        return null;
      }

      Dictionary<string, object> sqlDataRows = new Dictionary<string, object>();
      JObject modelJSON = new JObject();
      if(modelDataRow.Read()) {
        int columnOrdinalIndex = 0;
        // go though each column we put in and grab the result:
        foreach(string columnName in sqlColumns) {
          /// Json should be in the column named 'data'.
          if(columnName == Serializer.SQLJSONDataColumnName) {
            modelJSON = JObject.Parse(modelDataRow.GetString(columnOrdinalIndex++));
          } // otherwise it's a sql data row:
          else {
            sqlDataRows.Add(columnName, modelDataRow.GetValue(columnOrdinalIndex++));
          }
        }
      }

      modelDataRow.Close();
      query.Dispose();
      return (modelTableName, sqlDataRows, modelJSON);
    }

    /// <summary>
    /// Get the linked models to the given one:
    /// </summary>
    static Serializer.SerializedData? GetLinkedModelsInfo(
      string modelUniqueId,
      Serializer.SerializedData? existingData = null
    ) {
      NpgsqlCommand query = ModelDBConnection.CreateCommand();
      (IEnumerable<string> sqlColumns, string tableAssuranceQuery, _) = GetTableAssuranceQuery(LinkedModelDataFieldAttribute.LinksTableName, null);
      query.CommandText = tableAssuranceQuery
        + "SELECT " +
            sqlColumns.Join(SqlListDelimiter) + @"
          FROM " + LinkedModelDataFieldAttribute.LinksTableName + " " +
         "WHERE " + Serializer.SQLOwnerColumnName + " = @id";
      query.Parameters.AddWithValue("id", modelUniqueId);
      NpgsqlDataReader modelDataRow = query.ExecuteReader();

      // on failure:
      if(!modelDataRow.HasRows) {
        modelDataRow.Close();
        query.Dispose();
        return existingData;
      }

      Serializer.SerializedData @return = existingData ?? new Serializer.SerializedData();
      while(modelDataRow.Read()) {
        @return.addLink(
          modelDataRow.GetString(Serializer.SQLOwnerModelFieldNameColumnName),
          modelDataRow.GetString("childModelTable"),
          modelDataRow.GetString(Serializer.SQLIDColumnName)
        );
      }

      modelDataRow.Close();
      query.Dispose();
      return @return;
    }

    /// <summary>
    /// Get all the model data for a single child model type:
    /// </summary>
    static List<Serializer.SerializedData> GetDataForChildModels(
      string parentModelUniqueId,
      Type childModelType,
      string parentModelFieldName = null,
      string whereClause = null
    ) => GetDataForModels(childModelType, parentModelUniqueId, parentModelFieldName, whereClause);

    /// <summary>
    /// Get all the model data for a single child model type:
    /// </summary>
    static List<Serializer.SerializedData> GetDataForModels(
      Type modelType,
      string parentModelUniqueId = null,
      string parentModelFieldName = null,
      string whereAddendum = null,
      IEnumerable<(string name, object value)> extraParams = null,
      string fullWhereAndJoinClause = null,
      string fullSelect = null
    ) {
      // Get type data from Serializer.Cache:
      DataModelAttribute childModelInfo = Serializer.Cache.GetDataModelInfo(modelType);
      string modelTableName = childModelInfo.TableName;

      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> childModelFieldsInfo
        = Serializer.Cache.GetFieldsInfo(modelType);
      IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> sqlFieldsForThisModelType
        = childModelFieldsInfo.Where(field => field.dataModelFieldInfo.IsASQLField);
      (IEnumerable<string> sqlColumnsToQuery, string tableAssuranceQuery, _) 
        = GetTableAssuranceQuery(modelTableName, sqlFieldsForThisModelType);

      NpgsqlCommand query = ModelDBConnection.CreateCommand();

      // someone built it for us:
      if(!string.IsNullOrEmpty(fullSelect)) {
        query.CommandText = fullSelect;
      } // if we need to build it:
      else {
        bool useFullWhereAndJoinProvided = !string.IsNullOrEmpty(fullWhereAndJoinClause);
        bool hasParentField = !string.IsNullOrEmpty(parentModelFieldName);
        bool hasWhereClause = !string.IsNullOrEmpty(whereAddendum);
        bool hasOwnerId = !string.IsNullOrEmpty(parentModelUniqueId);

        bool hasWhere = hasParentField || hasWhereClause || hasOwnerId;

        query.CommandText = tableAssuranceQuery
          + "SELECT " +
              sqlColumnsToQuery.Join(SqlListDelimiter) + @"
          FROM " + modelTableName + " " +
            (useFullWhereAndJoinProvided
              ? fullWhereAndJoinClause :
              (hasWhere ? "WHERE " +
                (hasOwnerId ? $" {Serializer.SQLOwnerColumnName} = @owner {(hasParentField || hasWhereClause ? " AND " : "")}" : "") +
                (hasParentField ? $" {Serializer.SQLOwnerModelFieldNameColumnName} = @field {(hasWhereClause ? " AND " : "")}" : "") +
                (hasWhereClause ? $" {whereAddendum}" : "") : "")) + ";";

        if(hasParentField) {
          query.Parameters.AddWithValue("field", parentModelFieldName);
        }
        if(hasOwnerId) {
          query.Parameters.AddWithValue("owner", parentModelUniqueId);
        }
      }

      if(extraParams?.Any() ?? false) {
        extraParams.ForEach(
          param => query.Parameters.AddWithValue(param.name, param.value)
        );
      }
      NpgsqlDataReader resultingRows = query.ExecuteReader();

      // on failure:
      if(!resultingRows.HasRows) {
        resultingRows.Close();
        query.Dispose();
        return new List<Serializer.SerializedData>();
      }

      // reach each row into a list item
      List<Serializer.SerializedData> childModelDatas = new List<Serializer.SerializedData>();
      while(resultingRows.Read()) {
        int columnOrdinalIndex = 0;
        JObject jsonData = null;
        // go though each column we put in and grab the result:
        Dictionary<string, object> sqlDataRows = new Dictionary<string, object>();
        foreach(string columnName in sqlColumnsToQuery) {
          /// Json should be in a column named 'data':
          if(columnName == Serializer.SQLJSONDataColumnName) {
            jsonData = JObject.Parse(resultingRows.GetString(columnOrdinalIndex++));
          }
          else {
            sqlDataRows.Add(columnName, resultingRows.GetValue(columnOrdinalIndex++));
          }
        }

        childModelDatas.Add((
          modelTableName,
          sqlDataRows,
          jsonData
        ));
      }

      resultingRows.Close();
      query.Dispose();
      return childModelDatas;
    }

    /// <summary>
    /// Get the image for a model given the field the image is from
    /// </summary>
    static Dictionary<string, object> GetDataForModelImage(string modelId, string imageFieldName) {
      Dictionary<string, object> sqlRowData = new Dictionary<string, object>();
      // Make the sql command to fetch the image data
      NpgsqlCommand query = ModelDBConnection.CreateCommand();
      (IEnumerable<string> sqlColumnsToQuery, string tableAssuranceQuery, _) = GetTableAssuranceQuery(ModelImageFieldAttribute.ImageTableName, null);
      query.CommandText = tableAssuranceQuery
        + "SELECT " +
            sqlColumnsToQuery + @"
           FROM " + ModelImageFieldAttribute.ImageTableName + " " +
         $"WHERE {ModelImageFieldAttribute.ImageOwnerColumnName} = @imageOwner " +
           $"AND {ModelImageFieldAttribute.ImageOwnerFieldColumnName} = @imageField;";
      query.Parameters.AddWithValue("imageOwner", modelId);
      query.Parameters.AddWithValue("imageField", imageFieldName);

      // execute and read the query results:
      NpgsqlDataReader resultingRow = query.ExecuteReader();
      if (resultingRow.Read()) {
        int columnIndex = 0;
        foreach (string columnName in sqlColumnsToQuery) {
          sqlRowData.Add(columnName, resultingRow.GetValue(columnIndex++));
        }
      }

      resultingRow.Close();
      query.Dispose();
      return sqlRowData;
    }


    /// <summary>
    /// Save serialized model data to the db
    /// </summary>
    static void SaveModelDatas(Dictionary<string, Dictionary<string, ICollection<Serializer.SerializedData>>> serializedModelDatas) {
      int textParamId = 0;
      int linkParamId = 0;
      int linkParamNameId = 0;
      int paramNameId = 0;
      bool foundLink = false;

      // Lets build a BIG query lol
      NpgsqlCommand query = ModelDBConnection.CreateCommand();
      StringBuilder queryString = new StringBuilder();
      StringBuilder linkQueryString = new StringBuilder();
      List<NpgsqlParameter> linkQueryParams = new List<NpgsqlParameter>();

      // batch all the models that are in the same tables:
      string currentTable = null;
      IEnumerable<string> currentTableSqlColumnNames = default;
      Dictionary<string, Type> expectedColumnTypes = default;
      List<(string, string)> linkDeletes = null;

      /// for each table we need to add model data rows for:
      foreach ((string tableName, Dictionary<string, ICollection<Serializer.SerializedData>> serializedModelDatasById) in serializedModelDatas) {

        // if there's a query for another table already in progress we need to finish it:
        if (currentTable != null) {
          queryString.Append(";\n");
        }
        currentTable = tableName;

        // make sure to initialize this table:
        string tableAssuranceQuery;
        (currentTableSqlColumnNames, tableAssuranceQuery, expectedColumnTypes)
          = GetTableAssuranceQuery(currentTable, Serializer.Cache.GetSqlFieldsForTable(tableName));
        queryString.Append($"\n{tableAssuranceQuery}");

        // check if we have deletes for this table based on a parent id. If we do, do those first for the table:
        if (serializedModelDatasById.TryGetValue(Serializer.DeleteDataKey, out ICollection<Serializer.SerializedData> tableDeletionDatas)) {
          foreach (Serializer.SerializedData deletionData in tableDeletionDatas) {
            queryString.Append($"DELETE FROM {deletionData.tableName}\n");
            queryString.Append($"   WHERE {Serializer.SQLOwnerColumnName} = @param_{textParamId++}\n");
            queryString.Append($"      AND {Serializer.SQLOwnerModelFieldNameColumnName} = @param_{textParamId++};\n\n");

            query.Parameters.Add(new NpgsqlParameter {
              ParameterName = $"param_{ paramNameId++ }", 
              DbType = System.Data.DbType.String, 
              Value = deletionData.sqlData[Serializer.SQLOwnerColumnName],
              SourceColumn = Serializer.SQLOwnerColumnName 
            });
            query.Parameters.Add(new NpgsqlParameter{
              ParameterName = $"param_{paramNameId++}",
              DbType = System.Data.DbType.String, 
              Value = deletionData.sqlData[Serializer.SQLOwnerModelFieldNameColumnName],
              SourceColumn = Serializer.SQLOwnerModelFieldNameColumnName 
            });
          }

          // Remove the deletes:
          serializedModelDatasById.Remove(Serializer.DeleteDataKey);

          // if this was just a delete, continue without adding values
          if (!serializedModelDatasById.Any()) {
            continue;
          }
        }

        /// Add the rows as VALUEs
        // Initialize the upsert:
        queryString.Append($"INSERT INTO {currentTable} (\n\t");
        queryString.Append(currentTableSqlColumnNames.Join(SqlListDelimiter));
        queryString.Append($"\n) VALUES");

        // foreach set of model data under this id key add a row to the DB:
        int currentModelCount = 0;
        int totalModels = serializedModelDatasById.SelectMany(values => values.Value).Count();
        foreach ((string modelId, IEnumerable<Serializer.SerializedData> serializedDatas) in serializedModelDatasById) {
          foreach (Serializer.SerializedData serializedData in serializedDatas) {

            // if there's any links, add them to a collection for a seperate insert later:
            if(serializedData.linkedDataInfosByParentField?.Any() ?? false) {
              serializedData.linkedDataInfosByParentField.ForEach(entry =>
                entry.Value.ForEach(value => {
                  // if there's a delete link key:
                  if(value.id == Serializer.DeleteDataKey) {
                    linkDeletes ??= new List<(string, string)>();
                    linkDeletes.Add((entry.Key, serializedData.modelUniqueId));
                  }
                  else {
                    if(!foundLink) {
                      foundLink = true;
                      linkQueryString.Append($" (\n\t");
                    }
                    else {
                      linkQueryString.Append($", (\n\t");
                    }

                    int index = 0;
                    string[] columnValues = new[] {
                      // linked model id
                      value.id,
                      // parent model id
                      serializedData.modelUniqueId,
                      // field on the owner model
                      entry.Key,
                      // parent table name
                      serializedData.tableName,
                      // linked model table name
                      value.tableName
                    };
                    columnValues.ForEach(columnValue => {
                      index++;
                      linkQueryString.Append($"@param_{ linkParamId++ }");
                      if(index < columnValues.Length) {
                        linkQueryString.Append(SqlListDelimiter);
                      }
                      linkQueryParams.Add(new NpgsqlParameter() {
                        ParameterName = $"param_{ linkParamNameId++ }",
                        DbType = SQLColumnDataTypeMappings[typeof(string)],
                        Value = columnValue
                      });
                    });

                    linkQueryString.Append($"\n)");
                  }
                })
              );
            }

            if (currentModelCount++ != 0) {
              queryString.Append(",");
            }
            queryString.Append($" (\n\t");
            int currentRowAddedCount = 0;

            /// For each row of data to add in VALUES:
            // get all the columns and values we're using.
            currentTableSqlColumnNames
              // use null and rely on the DB to provide a default value:
              .Select(columnName => columnName != Serializer.SQLJSONDataColumnName 
                ? (name: columnName, value: serializedData.sqlData.TryGetValue(columnName, out object columnValue)
                  ? columnValue
                  : null)
                : (Serializer.SQLJSONDataColumnName, serializedData.jsonData?.ToString()))
              /// For each column of data to add in VALUES:
              .ForEach(column => {
                // add the VALUES ?s and +1 ? for json data too
                queryString.Append($"@param_{ textParamId++ }");
                // delimit if this isn't the last item in the list:
                if (++currentRowAddedCount != currentTableSqlColumnNames.Count()) {
                  queryString.Append(SqlListDelimiter);
                } // if it is the last item:
                else {
                  // if this is a model by unique id we want to upsert, not insert:
                  if (currentModelCount == totalModels && currentTableSqlColumnNames.First() == Serializer.SQLIDColumnName) {
                    // TODO: add a toggle that allows: on conflict, use JSON_MERGE_PATCH() to patch json changes onto the new one, to keep removed mod data.
                    queryString.Append($"\n) ON CONFLICT ({Serializer.SQLIDColumnName}) DO UPDATE SET\n");
                    int currentColumn = 1;
                    currentTableSqlColumnNames.Skip(1).ForEach(column => {
                      queryString.Append("\t");
                      queryString.Append(column);
                      queryString.Append(" = excluded.");
                      queryString.Append(column);
                      queryString.Append(++currentColumn != currentTableSqlColumnNames.Count() ? ",\n" : "");
                    });
                  } // the on conflict doesn't need a closing bracket, Values does
                  else {
                    queryString.Append($"\n)");
                  }
                }

                /// try to set up the sqlite param:
                try {
                  NpgsqlParameter param;
                  try {
                    param = new NpgsqlParameter { 
                      ParameterName = $"param_{ paramNameId++ }", 
                      DbType = SQLColumnDataTypeMappings[expectedColumnTypes[column.name]], 
                      Value = column.value ?? DBNull.Value,
                      SourceColumn = column.name
                    };
                  } catch (Exception e) {
                    throw new InvalidCastException($"Could not make a new SqliteParameter for value object of type: {column.value?.GetType().FullName ?? "null"}, with value: {column.value?.ToString() ?? "null"}. Tried to cast to DBtype: {(SQLColumnDataTypeMappings.TryGetValue(column.value?.GetType(), out var dbType) ? dbType.ToString() : "NO DBTYPE FOR THIS TYPE")}\n{e}");
                  }

                  query.Parameters.Add(param);
                } catch (InvalidCastException e) {
                  throw new InvalidCastException($"Could not cast object: {column.value?.ToString() ?? "null"}, of type: {column.value?.GetType().FullName ?? "null"}, to a type SQL can use.\n\nQuery:{query.CommandText}\n\nCurrentParamIndex:{query.Parameters.Count}\n\n{e}");
                }
              });
          }
        }
      }

      // Finalize and build the command string:
      queryString.Append(";");
      query.CommandText = queryString.ToString();

      query.ExecuteNonQuery();
      query.Dispose();

      /*string queryText = query.CommandText;

      foreach (NpgsqlParameter p in query.Parameters) {
        queryText = ReplaceFirst(
          queryText,
          string.IsNullOrEmpty(p.ParameterName) ? "?" : p.ParameterName,
          p.DbType == DbType.Int64 ? p.Value.ToString() : $"'{p.Value}'"
        );
      }*/

      if(foundLink) {
        //// Link Query:
        (IEnumerable<string> linkTableFieldNames, string linkTableAssuranceQuery, Dictionary<string, Type> linkTableDataTypes)
          = GetTableAssuranceQuery(LinkedModelDataFieldAttribute.LinksTableName, null);

        /// Add the rows as VALUEs
        // Initialize the insert:
        linkQueryString.Insert(0, $"\n) VALUES");
        linkQueryString.Insert(0, linkTableFieldNames.Join(SqlListDelimiter));
        linkQueryString.Insert(0, $"INSERT INTO {LinkedModelDataFieldAttribute.LinksTableName} (\n\t");

        // add link deletes
        // TODO: fix
        linkDeletes?.ForEach(linksToDeleteData => {
          linkQueryString.Insert(0, $"      AND {Serializer.SQLOwnerModelFieldNameColumnName} = @param_{linkParamId++};\n\n");
          linkQueryParams.Insert(0, new NpgsqlParameter {
            ParameterName = $"param_{ linkParamNameId++ }",
            DbType = System.Data.DbType.String,
            Value = linksToDeleteData.Item1,
            SourceColumn = Serializer.SQLOwnerColumnName
          });
          linkQueryString.Insert(0, $"   WHERE {Serializer.SQLOwnerColumnName} = @param_{linkParamId++}\n");
          linkQueryParams.Insert(0, new NpgsqlParameter {
            ParameterName = $"param_{linkParamNameId++}",
            DbType = System.Data.DbType.String,
            Value = linksToDeleteData.Item2,
            SourceColumn = Serializer.SQLOwnerModelFieldNameColumnName
          });
          linkQueryString.Insert(0, $"DELETE FROM {LinkedModelDataFieldAttribute.LinksTableName}\n");
        });

        // this should be the first thing:
        linkQueryString.Insert(0, "\n");
        linkQueryString.Insert(0, linkTableAssuranceQuery);

        NpgsqlCommand linkQuery = ModelDBConnection.CreateCommand();
        linkQuery.CommandText = linkQueryString.ToString();
        linkQuery.Parameters.AddRange(linkQueryParams.ToArray());

        linkQuery.ExecuteNonQuery();
        linkQuery.Dispose();
      }
    }

    /// <summary>
    /// testing, remove
    /// </summary>
    /// <param name="text"></param>
    /// <param name="search"></param>
    /// <param name="replace"></param>
    /// <returns></returns>
    static string ReplaceFirst(string text, string search, string replace) {
      int pos = text.IndexOf(search);
      if (pos < 0) {
        return text;
      }
      return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
    }

    #endregion

    #region Cache

    /// <summary>
    /// Serializer.Cached data queries
    /// </summary>
    static readonly Dictionary<string, (IEnumerable<string>, string, Dictionary<string, Type>)> CachedTableQueries
      = new Dictionary<string, (IEnumerable<string>, string, Dictionary<string, Type>)>();

    /// <summary>
    /// Get the columns to fetch for a table and a query to set up the table just in case
    /// </summary>
    static (
      IEnumerable<string> sqlColumnNames,
      string tableCreationAssuranceQuery,
      Dictionary<string, Type> columnDataTypes
    ) GetTableAssuranceQuery(
      string tableName,
      IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> sqlFieldsForThisModelType
    ) {
      IEnumerable<string> columnNames;
      Dictionary<string, Type> columnTypesByName
        = new Dictionary<string, Type>();
      string tableCreationAssuranceQuery;
      if (CachedTableQueries.TryGetValue(tableName, out var tableValues)) {
        (columnNames, tableCreationAssuranceQuery, columnTypesByName) = tableValues;
      } else {
        // image table columns:
        if (tableName == ModelImageFieldAttribute.ImageTableName) {
          columnTypesByName = ModelImageFieldAttribute.ImageTableColumns;
        }else if (tableName == LinkedModelDataFieldAttribute.LinksTableName) {
          columnTypesByName = LinkedModelDataFieldAttribute.LinksTableColumns;
        } // model defined columns:
        else {
          columnTypesByName = sqlFieldsForThisModelType.ToDictionary(
            field => field.dataModelFieldInfo.Name ?? field.objectFieldInfo.Name,
            field => field.dataModelFieldInfo.CustomSerializeToType ?? field.objectFieldInfo.DataType()
          );
          // data column:
          columnTypesByName[Serializer.SQLJSONDataColumnName] = typeof(string);
        }

        int currentIndex = 0;

        // Make and organize the column order list:
        List<string> columnsToFetch;
        columnsToFetch = columnTypesByName.Keys.ToList();
        if(columnTypesByName.ContainsKey(Serializer.SQLIDColumnName)) {
          columnsToFetch.Move(Serializer.SQLIDColumnName, currentIndex++);
        }
        if(columnTypesByName.ContainsKey(Serializer.SQLTypeColumnName)) {
          columnsToFetch.Move(Serializer.SQLTypeColumnName, currentIndex++);
        }
        if(columnTypesByName.ContainsKey(Serializer.SQLOwnerColumnName)) {
          columnsToFetch.Move(Serializer.SQLOwnerColumnName, currentIndex++);
        }
        if(columnTypesByName.ContainsKey(Serializer.SQLOwnerModelFieldNameColumnName)) {
          columnsToFetch.Move(Serializer.SQLOwnerModelFieldNameColumnName, currentIndex++);
        }
        if(columnTypesByName.ContainsKey(Serializer.SQLJSONDataColumnName)) {
          columnsToFetch.Move(Serializer.SQLJSONDataColumnName, currentIndex++);
        }
        if(columnTypesByName.ContainsKey(Serializer.SQLComponentsDataColumnName)) {
          columnsToFetch.Move(Serializer.SQLComponentsDataColumnName, currentIndex++);
        }
        columnNames = columnsToFetch;

        // query text generation:
        tableCreationAssuranceQuery
          = $"CREATE TABLE IF NOT EXISTS {tableName} (\n\t" +
              columnNames.Select(columnName => $"{columnName} {(PostgreSqlColumnDataTypeMappings.TryGetValue(columnTypesByName[columnName], out string columnTypeName) ? columnTypeName : throw new NotImplementedException($"type {columnTypesByName[columnName]} is not recoginized as a SQL column type in SQLColumnDataTypeMappings"))} {(columnName == Serializer.SQLIDColumnName ? "PRIMARY KEY " : "")}").Join(SqlListDelimiter) +
            $"\n);\n\n";

        // no pk in the link table:
        if (tableName == LinkedModelDataFieldAttribute.LinksTableName) {
          tableCreationAssuranceQuery = tableCreationAssuranceQuery.Replace("PRIMARY KEY ", "");
        }
      }

      CachedTableQueries[tableName] = (columnNames, tableCreationAssuranceQuery, columnTypesByName);
      return (columnNames, tableCreationAssuranceQuery, columnTypesByName);
    }

    #endregion
  }
}
